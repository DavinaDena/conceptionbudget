# Project Budget

For this project I had to design diagrams for the creation of an application to manage my budget.

### Use case

For starters, I decided to create the entity User since in the future I might have more than just one person to use the application.
After creating an account and login, the User can:
- check the account balance
- add an entry
- add a withdrawal

The user can also check a single operation (can be a withdrawal or an entry) and categorize it so later he can check his dispenses based by category.
The User can also freeze an operation, making it impossible to touch.

When checking the balance, the User can:
- check by category -> so the user can see how much he dispensed based on a category
- check by entry -> the User can check all the money that have entered on his account
- check by withdrawl -> the User can check all the money that left the account 
- check by operation -> the User can check a specific amount of money, his category and the date
- check by date -> the User can check all the operations that were made on a specific date


When the User check a single operation, the User has the options to categorize the operation or freeze, making it impossible to edit.

Since its an application for personal use, all the operations and details can be edited, deleted or we can add new ones.


### Class Diagram

In the class diagram, I decided to create six tables:
- User
- Budget
- Money
- Withdrawal
- Category
- Entry

User since in the future we might have more than a person to use the application.
Budget -> table to show the amount of money on the account
Money -> single operation, might be positive if its an entry or negative if its a withdrawal
Withdrawal-> table to show all the money that has left the account
Entry -> all the money that has entered the account
Category -> a table to save all the categories that we can associate to an operation.

I thought of separating the Budget and Money since I found it easier to separate both so we can manipulate better a single operation than having everything in one table.

Made Withdrawl and Entry as separated tables for the same purpose, although I thought of leaving it on a single table (Money) to not mix on the same table negative and positive amounts.

Made the Category table to be easier to assign them to the operations than having it as a property of the table Money (to avoid repetition and easier to class operations by category)


### Data model

Well... something? Still didn't understand how doing it correctly nor manage to do it correctly. Can't explain something that I don't really know right?


As for the "extend" and "include", same, still can't vision it and assign it.


Didn't do the bonus since it took me some time already to understand the difference between MVC and data model ( although I can see the difference, can't still put it to practise).
I also find what I did not the best and rather confusing, but... can't think of a better way :v






